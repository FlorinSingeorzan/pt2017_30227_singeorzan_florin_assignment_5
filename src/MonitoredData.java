import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

class MonitoredData {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String label;



    MonitoredData(String startTime,String endTime,String label){
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.startTime=LocalDateTime.parse(startTime,formatter);
        this.endTime=LocalDateTime.parse(endTime,formatter);
        this.label=label;
    }

     LocalDateTime getStartTime() {
        return startTime;
    }


     String getLabel() {
        return label;
    }

    int timeBetween(){
        LocalDateTime time=LocalDateTime.from(this.startTime);

        return (int) time.until(this.endTime, ChronoUnit.SECONDS);
    }



}
