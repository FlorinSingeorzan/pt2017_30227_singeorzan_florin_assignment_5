import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Activities {
    private List<MonitoredData> activities;
    private Map<String,Long> distinctAction;
    private Map<Integer,Map<String,Long>> eachDay;
    private Map<String,Integer> durationActivities;
    private List<String> shortActivities;


    Activities(){
        activities = new ArrayList<>();
        distinctAction = new HashMap<>();
        durationActivities = new HashMap<>();
        eachDay = new HashMap<>();
        shortActivities = new ArrayList<>();
        readFile();
        countDays();
        try {
            distinctActions();
            largerPeriodDuration();
            distinctPerDay();
            predominantActivities();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFile(){
        try {
            FileReader file=new FileReader("activities.txt");
            BufferedReader reader=new BufferedReader(file);
            String start;
            String end;
            String label;
            Scanner read=new Scanner(reader);
            while (read.hasNext()) {
                start = read.next() + " " + read.next();
                end=read.next()+" "+read.next();
                label=read.next();
                activities.add(new MonitoredData(start,end,label));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void countDays(){
        Long days=activities.stream()
                .map((MonitoredData s) -> s.getStartTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).substring(0,10))
                .distinct()
                .count();
        System.out.println("Number of days: "+days);
    }


    private void distinctActions() throws IOException{
        distinctAction=activities.stream()
                .collect(Collectors.groupingBy(MonitoredData::getLabel,Collectors.counting()));

        BufferedWriter bufferedWriter=create("distinctActions.txt");
        distinctAction.forEach((String label, Long number) -> {
            try {
                bufferedWriter.write(label + "       ->       " + number+";    ");
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        bufferedWriter.close();
    }

    private void distinctPerDay() throws IOException {
        eachDay=activities.stream()
                .collect(Collectors.groupingBy(((MonitoredData s)->s.getStartTime().getDayOfMonth()),Collectors.groupingBy(MonitoredData::getLabel,Collectors.counting())));
        BufferedWriter writer=create("distinctActivitiesPerDay.txt");
        eachDay.forEach((key1, value1) -> {
            try {
                writer.write(key1 + "  ->   ");
                value1.forEach((key, value) -> {
                    try {
                        writer.newLine();
                        writer.write(key + " - " + value);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                writer.newLine();
            } catch (IOException ignored) {
            }

        });
        writer.close();

    }

    private void largerPeriodDuration() throws IOException {
        durationActivities=activities.stream()
                .collect(Collectors.groupingBy(MonitoredData::getLabel,Collectors.reducing(0,MonitoredData::timeBetween,Integer::sum)))
                .entrySet().stream()
                .filter(s-> s.getValue()/3600>=10)
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
        BufferedWriter writer=create("largerActivities.txt");
        durationActivities.forEach((String label,Integer duration)->{
            try {
                writer.write(label + "       ->       " + duration/3600+";    ");
                writer.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.close();
    }

    private BufferedWriter create(String name) throws IOException {
        File file= new File(name);
        if(!file.exists()){
            //noinspection ResultOfMethodCallIgnored
            file.createNewFile();
        }
        FileWriter writer=new FileWriter(file);
        return new BufferedWriter(writer);
    }

    private void  predominantActivities() throws IOException {
        Map<String ,Map<Boolean,List<MonitoredData>>> predominant;
        predominant = activities.stream()
                .collect(Collectors.groupingBy(MonitoredData::getLabel,Collectors.partitioningBy(s->s.timeBetween()<=300)))
                .entrySet().stream()
                .filter(s->(float)(s.getValue().get(true).size())/(float) (s.getValue().get(true).size()+s.getValue().get(false).size())>=0.9f)
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));

        shortActivities.addAll(predominant.keySet());
        BufferedWriter writer=create("shortActivities.txt");
        shortActivities.forEach((String label)->{
            try {
                writer.write(label);
                writer.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.close();
    }
}


